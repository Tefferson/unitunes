/**
 * @providesModule Assets
 */

const Images = {
    imagePlaceholder: require('./images/image-placeholder.png')
}

export {Images}