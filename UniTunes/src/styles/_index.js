/**
 * @providesModule Styles
 */

 import Colors from './Colors'

 export {
     Colors
 }