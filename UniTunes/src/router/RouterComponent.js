/**
 * @providesModule Router
 */

import React from 'react'
import {Root} from 'native-base'
import {createDrawerNavigator, createStackNavigator} from 'react-navigation'

import SettingsStackNavigator from './settings/SettingsStackNavigator'
import LibraryStackNavigator from './library/LibraryStackNavigator'
import TrackStackNavigator from './track/TrackStackNavigator'
import FundsStackNavigator from './funds/FundsStackNavigator'
import {HomeScreen, LoginScreen} from 'Screens'
import {NavigationService} from 'Services'

export default class RouterComponent extends React.Component {
    render() {
        return (
            <Root>
                <AppRoot ref={NavigationService.setTopLevelNavigator}/>
            </Root>
        )
    }
}

const HomeDrawer = createDrawerNavigator({
    Home: {
        screen: HomeScreen
    },
    Library: {
        screen: LibraryStackNavigator,
        navigationOptions: ({navigation}) => ({title: 'Biblioteca'})
    },
    Funds: {
        screen: FundsStackNavigator,
        navigationOptions: ({navigation}) => ({title: 'Saldo'})
    },
    Settings: {
        screen: SettingsStackNavigator,
        navigationOptions: ({navigation}) => ({title: 'Configurações'})
    }
}, {initialRouteName: 'Home'})

const AppRoot = createStackNavigator({
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            header: null
        }
    },
    Home: {
        screen: HomeDrawer,
        navigationOptions: {
            header: null
        }
    },
    Track: {
        screen: TrackStackNavigator,
        navigationOptions: ({navigation}) => {
            const {artist, name} = navigation.state.params
            if (artist !== undefined && name !== undefined) 
                return {title: `${name} - ${artist}`}
            }
    }
}, {initialRouteName: 'Login'})