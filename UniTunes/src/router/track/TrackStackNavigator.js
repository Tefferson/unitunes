import {createStackNavigator} from 'react-navigation'

import * as TrackScreens from 'TrackScreens'

export default createStackNavigator({
    Info: {
        screen: TrackScreens.InfoScreen,
        navigationOptions: {
            header: null
        }
    }
}, {initialRouteName: 'Info'})