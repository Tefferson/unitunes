import {createStackNavigator} from 'react-navigation'

import * as SettingsScreens from 'SettingsScreens'

export default createStackNavigator({
    Settings: {
        screen: SettingsScreens.SettingsScreen,
        navigationOptions: {
            header: null
        }
    }
}, {initialRouteName: 'Settings'})