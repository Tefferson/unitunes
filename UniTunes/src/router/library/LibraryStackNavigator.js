import {createStackNavigator} from 'react-navigation'

import * as LibraryScreens from 'LibraryScreens'

export default createStackNavigator({
    Library: {
        screen: LibraryScreens.LibraryScreen,
        navigationOptions: {
            header: null
        }
    }
}, {initialRouteName: 'Library'})