import {createStackNavigator} from 'react-navigation'

import * as FundsScreens from 'FundsScreens'

export default createStackNavigator({
    Funds: {
        screen: FundsScreens.FundsScreen,
        navigationOptions: {
            header: null
        }
    }
}, {initialRouteName: 'Funds'})