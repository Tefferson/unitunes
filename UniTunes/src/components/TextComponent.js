import React from 'react'
import {Text} from 'react-native'

export default class TextComponent extends React.Component {
    constructor(props) {
        super(props)
        this.style = {
            color: getColor(props),
            fontStyle: getFontStyle(props),
			textAlign: getTextAlign(props)
        }
    }

    render() {
        const {props, style} = this
        return <Text style={[style, props.style]}>{props.children}</Text>
    }
}

function getFontStyle({ italic }) {
	if (italic !== undefined) return 'italic'
	else return 'normal'
}

function getColor({forButton, balihai, white}) {
    if (forButton !== undefined || white !== undefined) 
        return '#ffffff'
    else if (balihai !== undefined) 
        return '#8097ab'
    else 
        return '#4f4f4f'
}

function getTextAlign({ auto, center, justify, left, right }) {
	if (auto !== undefined) return 'auto'
	else if (center !== undefined) return 'center'
	else if (justify !== undefined) return 'justify'
	else if (left !== undefined) return 'left'
	else if (right !== undefined) return 'right'
	else return null
}