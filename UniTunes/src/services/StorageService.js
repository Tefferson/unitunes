const keys = {
    funds: 'schedule',
    tracks: 'tracks',
    user: 'user',
    // TODO: se der tempo 	rememberPassword: 'rememberpassword', 	credentials:
    // 'credentials', 	didFirstAccess: 'didfirstaccess',
}

const deleteItemAsync = key => Expo
    .SecureStore
    .deleteItemAsync(key)

const getItemAsync = async key => {
    const item = await Expo
        .SecureStore
        .getItemAsync(key)
    return item === null || item === undefined
        ? null
        : JSON.parse(item)
}

const user = {
    getAsync: () => getItemAsync(keys.user),
    deleteAsync: () => deleteItemAsync(keys.user),
    setAsync: user => Expo
        .SecureStore
        .setItemAsync(keys.user, JSON.stringify(user))
}

const tracks = {
    getAsync: userId => getItemAsync(keys.tracks + userId),
    setAsync: (tracks, userId) => Expo
        .SecureStore
        .setItemAsync(keys.tracks + userId, JSON.stringify(tracks))
}

const funds = {
    getAsync: userId => getItemAsync(keys.funds + userId),
    setAsync: (funds, userId) => Expo
        .SecureStore
        .setItemAsync(keys.funds + userId, JSON.stringify(funds))
}

export default {
    funds,
    tracks,
    user
}