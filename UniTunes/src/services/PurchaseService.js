import {StorageService} from 'Services'

const buyTrackAsync = ({name, artist, title, duration}) => {
    const price = ~~(duration / 1000)
    const {user, tracks} = StorageService
    return new Promise(resolve => {
        user
            .getAsync()
            .then(({userId}) => StorageService.funds.getAsync(userId).then(funds => {
                if (!funds || funds < price) 
                    resolve(false)
                else {
                    const fundsPromise = StorageService
                        .funds
                        .setAsync(funds - price, userId)
                    const tracksPromise = tracks
                        .getAsync(userId)
                        .then(async _tracks => {
                            const tracksData = Array.isArray(_tracks)
                                ? _tracks
                                : []
                            const key = ('' + name + artist + title + duration).replace(/[^a-zA-Z0-9]/g, '0')
                            tracksData.push({key, name, artist, title, duration})
                            await tracks.setAsync(tracksData, userId)
                        })
                    Promise
                        .all([fundsPromise, tracksPromise])
                        .then(() => resolve(true))
                }
            }))
    })
}

const isPurchasedAsync = ({name, artist, album, duration}) => {
    const {user, tracks} = StorageService
    const _key = ('' + name + artist.name + ((album || {}).title) + duration).replace(/[^a-zA-Z0-9]/g, '0')
    return new Promise(resolve => {
        user
            .getAsync()
            .then(({userId}) => tracks.getAsync(userId).then(_tracks => {
                if (Array.isArray(_tracks)) {
                    resolve(_tracks.some(({key}) => key === _key))
                } else {
                    resolve(false)
                }
            }))
    })
}

export default {
    buyTrackAsync,
    isPurchasedAsync
}