import {StorageService} from 'Services'

const getFundsAsync = () => {
    const {user, funds} = StorageService
    return new Promise(resolve => {
        user
            .getAsync()
            .then(({userId}) => funds.getAsync(userId).then(funds => resolve(funds || 0)))
    })
}

const addFundsAsync = value => {
    const {user, funds} = StorageService
    return new Promise(resolve => getFundsAsync().then(currentBalance => user.getAsync().then(({userId}) => funds.setAsync(currentBalance + value * 100, userId).then(() => resolve()))))
}

export default {
    getFundsAsync,
    addFundsAsync
}