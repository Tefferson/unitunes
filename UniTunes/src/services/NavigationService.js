import {NavigationActions} from 'react-navigation'

let navigator = null

const navigate = routeName => params => navigator
    ._navigation
    .navigate(routeName, params)

const setTopLevelNavigator = navigatorRef => (navigator = navigatorRef)

const home = {
    navigateTo: navigate('Home')
}

const login = {
    navigateTo: navigate('Login')
}

const track = {
    pushInfo: navigate('Track')
}

export default {
    setTopLevelNavigator,
    home,
    login,
    track
}