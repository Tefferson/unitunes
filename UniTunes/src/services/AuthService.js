import {StorageService} from 'Services'

const isAuthenticatedAsync = () => StorageService
    .user
    .getAsync(user => user !== null)

const authenticateAsync = ({username, password}) => {
    const user = {
        username,
        password,
        userId: (username + password).replace(/[^a-zA-Z0-9]/g, '0')
    }
    return StorageService
        .user
        .setAsync(user)
}

const logoutAsync = () => StorageService
    .user
    .deleteAsync()

export default {
    authenticateAsync,
    isAuthenticatedAsync,
    logoutAsync
}