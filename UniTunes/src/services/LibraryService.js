import {StorageService} from 'Services'

const getTracksAsync = () => {
    const {user, tracks} = StorageService
    return user
        .getAsync()
        .then(({userId}) => tracks.getAsync(userId).then(tracks => Array.isArray(tracks)
            ? tracks
            : []))
}

export default {
    getTracksAsync
}