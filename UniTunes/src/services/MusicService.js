import axios from 'axios'

const baseUrl = 'http://ws.audioscrobbler.com/2.0/'

export default class MusicService {
    constructor() {
        this.apiKey = 'f43dc1b838a74a800f48a59f68ebcf2d'
    }

    searchTrack({term}) {
        return this
            .get({
            method: 'track.search',
            params: {
                track: term
            }
        })
            .then(({data}) => data.results.trackmatches.track)
    }

    getInfoTrack({artist, track}) {
        return this
            .get({
            method: 'track.getInfo',
            params: {
                artist,
                track
            }
        })
            .then(({data}) => data.track)
    }

    get(data) {
        const params = {
            api_key: this.apiKey,
            format: 'json',
            method: data.method,
            ...data.params
        }
        return axios.get(`${baseUrl}`, {params})
    }
}