import Rx from 'rxjs/Rx'

export default class SearchService {
    constructor({endpoint}) {
        this.endpoint = endpoint
        this.searchTerm = new Rx.Subject()
    }

    search(term) {
        this
            .searchTerm
            .next(term)
    }

    doSearch(term) {
        return Rx
            .Observable
            .fromPromise(this.endpoint({term}).catch(error => []))
    }

    getResults() {
        return this
            .searchTerm
            .debounceTime(500)
            .switchMap(term => term === undefined
                ? Rx.Observable.of([])
                : this.doSearch(term))
            .catch(error => {
                return Rx
                    .Observable
                    .of([])
            })
    }
}
