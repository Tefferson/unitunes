import React from 'react'
import {
    Dimensions,
    Image,
    View,
    Text,
    StatusBar,
    StyleSheet
} from 'react-native'
import {
    Button,
    Container,
    Content,
    Form,
    Item,
    Input,
    Label,
    Toast
} from 'native-base'
import {Header} from 'react-navigation'

import {Images} from 'Assets'
import {TextComponent} from 'Components'
import {MusicService, NavigationService, PurchaseService} from 'Services'

export default class InfoScreen extends React.Component {

    constructor(props) {
        super(props)
        this.musicService = new MusicService()
        this.state = {
            track: null,
            bought: false
        }
    }

    componentDidMount() {
        const {name, artist} = this.props.navigation.state.params
        this
            .musicService
            .getInfoTrack({artist, track: name})
            .then(track => {
                PurchaseService
                    .isPurchasedAsync(track)
                    .then(bought => this.setState({bought}))
                this.setState({track})
            })
    }

    render() {
        if (this.state.track === null) 
            return null
        const {album, duration, name, toptags} = this.state.track
        const {artist, image, title} = album || {
            artist: '',
            title: ''
        }
        const uri = image.find(({size}) => size === 'extralarge')['#text']
        const source = uri
            ? {
                uri
            }
            : Images.placeholderImage
        const price = calculatePrice(duration)
        return (
            <Container>
                <Content
                    contentContainerStyle={{
                    alignItems: 'center'
                }}>
                    <Image style={styles.image} source={source}/>
                    <TextComponent style={styles.textInfo}>
                        {name}{' - '}{artist}{' '}
                        <TextComponent style={styles.textDuration} balihai>{getDuration(duration)}</TextComponent>
                    </TextComponent>
                    <TextComponent style={styles.albumTitle} italic>{title}</TextComponent>
                    <View>{renderTags.call(this, toptags)}</View>
                    {this.state.bought
                        ? <Button style={styles.button} success>
                                <TextComponent forButton>Adquirido</TextComponent>
                            </Button>
                        : (
                            <Button
                                onPress={onPress.bind(this, name, artist, title, duration)}
                                style={styles.button}>
                                <TextComponent forButton>{'Comprar '}{price}</TextComponent>
                            </Button>
                        )}
                </Content>
            </Container>
        )
    }
}

function calculatePrice(duration) {
    const price = ~~(duration / 1000)
    const cents = price % 100
    return `R$ ${ ~~ (price / 100)},${cents < 10
        ? '0'
        : ''}${cents}`
}

function getDuration(millis) {
    const seconds = ~~(millis / 1000)
    const minutes = ~~(seconds / 60)
    const remains = seconds % 60
    return `${minutes}:${remains < 10
        ? '0'
        : ''}${remains}`
}

function renderTags({tag}) {
    return (
        <View style={styles.tagsContainer}>
            {tag.map(({
                name
            }, index) => (
                <TextComponent style={styles.tag} key={index} white>{name}</TextComponent>
            ))}
        </View>
    )
}

function onPress(name, artist, title, duration) {
    PurchaseService
        .buyTrackAsync({name, artist, title, duration})
        .then(bought => {
            if (bought) {
                Toast.show({text: 'Compra efetuada', buttonText: 'OK', duration: 3000, type: 'success'})
                this.setState({bought})
            } else {
                Toast.show({text: 'Créditos insuficientes', buttonText: 'OK', duration: 3000, type: 'warning'})
            }
        })
}

const {width} = Dimensions.get('window')

const styles = StyleSheet.create({
    albumTitle: {
        fontSize: 12,
        marginTop: 4
    },
    button: {
        paddingHorizontal: 15,
        marginTop: 25,
        alignSelf: 'center'
    },
    image: {
        height: width,
        width
    },
    tag: {
        backgroundColor: '#336699',
        borderRadius: 3,
        margin: 2,
        paddingVertical: 2,
        paddingHorizontal: 6
    },
    tagsContainer: {
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 10
    },
    textDuration: {},
    textInfo: {
        fontSize: 16,
        marginTop: 10
    }
})