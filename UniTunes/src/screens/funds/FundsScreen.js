import React from 'react'
import {View, Text, StatusBar, StyleSheet} from 'react-native'
import {Content, Container, Form, Button, Toast} from 'native-base'

import {HeaderComponent, TextComponent} from 'Components'
import {FundsService, NavigationService} from 'Services'

export default class SettingsScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            funds: 0
        }
    }

    componentDidMount() {
        FundsService
            .getFundsAsync()
            .then(funds => this.setState({funds}))
    }

    render() {
        const {funds} = this.state
        return (
            <Container>
                <HeaderComponent title={'Saldo'}/>
                <Content contentContainerStyle={styles.content}>
                    <TextComponent
                        style={{
                        fontSize: 14
                    }}
                        center
                        balihai>Você possui</TextComponent>
                    <TextComponent
                        style={{
                        fontSize: 24
                    }}
                        center>{calculateFunds(funds)}</TextComponent>
                    <TextComponent
                        style={{
                        marginTop: 25,
                        marginBottom: 10,
                        fontSize: 18
                    }}
                        center>Adicionar saldo</TextComponent>
                    <View style={styles.buttonsContainer}>
                        <Button onPress={addFunds.bind(this, 5)} style={styles.addButton} info>
                            <TextComponent forButton center>R$ 5,00</TextComponent>
                        </Button>
                        <Button onPress={addFunds.bind(this, 20)} style={styles.addButton} primary>
                            <TextComponent forButton center>R$ 20,00</TextComponent>
                        </Button>
                        <Button onPress={addFunds.bind(this, 50)} style={styles.addButton} danger>
                            <TextComponent forButton center>R$ 50,00</TextComponent>
                        </Button>
                        <Button onPress={addFunds.bind(this, 100)} style={styles.addButton} dark>
                            <TextComponent forButton center>R$ 100,00</TextComponent>
                        </Button>
                    </View>
                </Content>
            </Container>
        )
    }
}

function addFunds(value) {
    FundsService
        .addFundsAsync(value)
        .then(() => {
            this.setState({
                funds: this.state.funds + value * 100
            })
            Toast.show({text: 'Fundos adicionados', buttonText: 'OK', duration: 3000, type: 'success'})
        })
}

function calculateFunds(value) {
    const cents = value % 100
    return `R$ ${ ~~ (value / 100)},${cents > 9
        ? ''
        : '0'}${cents}`
}

const styles = StyleSheet.create({
    addButton: {
        justifyContent: 'center',
        paddingHorizontal: 15,
        margin: 6,
        width: '40%'
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        flexWrap: 'wrap'
    },
    content: {
        marginTop: 25
    }
})