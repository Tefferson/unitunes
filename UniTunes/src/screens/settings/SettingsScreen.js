import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {Content, Container, Button, Icon} from 'native-base'

import {HeaderComponent, TextComponent} from 'Components'
import {AuthService, NavigationService} from 'Services'

export default class SettingsScreen extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Container>
                <HeaderComponent title={'Configurações'}/>
                <Content>
                    <Button onPress={onPress.bind(this)} style={styles.button}>
                        <TextComponent forButton>Sair</TextComponent>
                        <Icon name={'exit'}/>
                    </Button>
                </Content>
            </Container>
        )
    }
}

function onPress() {
    AuthService
        .logoutAsync()
        .then(() => NavigationService.login.navigateTo())
}

const styles = StyleSheet.create({
    button: {
        alignSelf: 'center',
        marginTop: 25,
        paddingLeft: 14
    }
})