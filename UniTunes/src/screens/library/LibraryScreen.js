import React from 'react'
import {View, Text, StyleSheet, FlatList, Status} from 'react-native'
import {Container, Content} from 'native-base'

import {HeaderComponent, TextComponent} from 'Components'
import {LibraryService, NavigationService} from 'Services'

export default class LibraryScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tracks: []
        }
    }

    componentDidMount() {
        LibraryService
            .getTracksAsync()
            .then(tracks => this.setState({tracks}))
    }

    render() {
        return (
            <Container>
                <HeaderComponent title={'Biblioteca'}/>
                <Content>
                    <FlatList
                        data={this.state.tracks}
                        renderItem={renderItem.bind(this)}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={(
                        <TextComponent center>Está meio vazio por aqui</TextComponent>
                    )}
                        ItemSeparatorComponent={() => (<View
                        style={{
                        backgroundColor: '#b0b0b0',
                        height: 0.8,
                        width: '100%'
                    }}/>)}/>
                </Content>
            </Container>
        )
    }
}

function renderItem({item}) {
    const {name, title, duration, artist} = item
    return (
        <View
            style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
            paddingHorizontal: 5
        }}>
            <TextComponent style={styles.text}>{name}{' - '}{artist}</TextComponent>
            <TextComponent style={styles.text} balihai>{getDuration(duration)}</TextComponent>
        </View>
    )
}

function getDuration(value) {
    const seconds = value / 1000 % 60
    const minutes = ~~(value / 1000 / 60)
    return `${minutes}:${seconds > 9
        ? ''
        : '0'}${seconds}`
}

const styles = StyleSheet.create({
    text: {
        fontSize: 18
    }
})