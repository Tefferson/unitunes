import React from 'react'
import {
    Dimensions,
    FlatList,
    Image,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    StyleSheet
} from 'react-native'
import {
    Button,
    Container,
    Header,
    Content,
    Form,
    Item,
    Input,
    Label
} from 'native-base'
import Rx from 'rxjs/Rx'

import {Images} from 'Assets'
import {HeaderComponent, TextComponent} from 'Components'
import {MusicService, NavigationService, SearchService} from 'Services'

export default class HomeScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tracks: [],
            query: ''
        }
        this.musicService = new MusicService()
        this.searchService = new SearchService({
            endpoint: ({term}) => this
                .musicService
                .searchTrack({term})
        })
        this.searchSubscription = new Rx.Subscription()
    }

    componentDidMount() {
        this.searchSubscription = this
            .searchService
            .getResults()
            .subscribe(tracks => this.setState({tracks}))
    }

    componentWillUnmount() {
        this
            .searchSubscription
            .unsubscribe()
    }

    render() {
        return (
            <Container>
                <HeaderComponent title={'Home'}/>
                <Content>
                    <Form>
                        <Item last>
                            <Label>Pesquisar</Label>
                            <Input onChangeText={onChangeText.bind(this)}/>
                        </Item>
                    </Form>
                    <FlatList
                        data={this.state.tracks}
                        renderItem={renderItem.bind(this)}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={(
                        <TextComponent center>Faça uma busca...</TextComponent>
                    )}/>
                </Content>
            </Container>
        )
    }
}

function renderItem({item}) {
    const {name, artist, image} = item
    const uri = image.find(({size}) => size === 'large')['#text']
    const source = uri
        ? {
            uri
        }
        : Images.imagePlaceholder
    return (
        <TouchableOpacity
            activeOpacity={0.9}
            style={styles.item}
            onPress={itemPress.bind(this, item)}>
            <Image style={styles.itemImage} source={source}/>
            <TextComponent center>{name}{' - '}{artist}</TextComponent>
        </TouchableOpacity>
    )
}

function itemPress({name, artist}) {
    NavigationService
        .track
        .pushInfo({name, artist})
}

function onChangeText(query) {
    this
        .searchService
        .search(query)
    this.setState({query})
}

const {width} = Dimensions.get('window')

const styles = StyleSheet.create({
    item: {
        height: 300,
        alignItems: 'center',
        justifyContent: 'center',
        width
    },
    itemImage: {
        height: 240,
        width: 240
    }
})