import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {
    Button,
    Container,
    Header,
    Content,
    Form,
    Item,
    Input,
    Label,
    Toast
} from 'native-base'

import {TextComponent} from 'Components'
import {AuthService, NavigationService} from 'Services'

export default class LoginScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            loading: true
        }
    }

    componentDidMount() {
        AuthService
            .isAuthenticatedAsync()
            .then(isAuthenticated => {
                if (isAuthenticated) {
                    NavigationService
                        .home
                        .navigateTo()
                } else {
                    this.setState({loading: false})
                }
            })
    }

    render() {
        if (this.state.loading) 
            return null
        return (
            <Container>
                <Content
                    contentContainerStyle={{
                    marginTop: 50,
                    paddingHorizontal: 25
                }}>
                    <Form>
                        <Item floatingLabel>
                            <Label>Usuário</Label>
                            <Input
                                value={this.state.username}
                                onChangeText={username => this.setState({username})}/>
                        </Item>
                        <Item floatingLabel last>
                            <Label>Senha</Label>
                            <Input
                                value={this.state.password}
                                onChangeText={password => this.setState({password})}
                                secureTextEntry/>
                        </Item>
                    </Form>
                    <Button
                        style={{
                        marginTop: 50
                    }}
                        onPress={onPress.bind(this)}
                        full>
                        <TextComponent forButton>Entrar</TextComponent>
                    </Button>
                </Content>
            </Container>
        )
    }
}

function onPress() {
    const {username, password} = this.state
    if (username && password) {
        AuthService
            .authenticateAsync({username, password})
            .then(() => this.setState({
                username: '',
                password: ''
            }, () => NavigationService.home.navigateTo()))
    } else {
        Toast.show({text: 'Informe as credenciais', buttonText: 'OK', duration: 3000, type: 'warning'})
    }
}